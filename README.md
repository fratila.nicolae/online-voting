The application can be run from inside a IDE (I recommend Intellij) or from command line by typing "mvn spring-boot:run"
After that, the application will be available at http://localhost:8080 and an in-memory database will be available at http://localhost:8080/h2-console

How to connect to database:
* access http://localhost:8080/h2-console
* fill the fields as follows:
    * Driver: org.h2.Driver
    * JDBC URL: jdbc:h2:mem:bootapp;DB_CLOSE_DELAY=-1
    * User Name: sa
    * Password:

      Note - password is blank

* click Connect
    
After that you should see in top left the database, with a table called VOTER and have in the middle top of the screen a sql console for all your needs.
    
The data that appears in the database can be found in src/main/resources/data.sql
