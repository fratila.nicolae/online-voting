package com.ubb.cripto.voting;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.Arrays;

@Controller
public class SimpleController {

  @Autowired
  private VoterRepository voterRepository;

  @GetMapping("/")
  public String homePage(Model model) {
    // pentru a trimite date la view trebuie sa le adaugi in model.
    // home e numele template-ului. template-urile sunt in src/main/java/resources/templates,
    // locatie configurata in src/main/java/resources/application.properties drept spring.thymeleaf.prefix
    return goToVotePage(model);
  }

  private String goToVotePage(Model model){
    model.addAttribute("voter", new Voter());
    model.addAttribute("candidates", Arrays.asList("candidate1", "candidate2", "candidate3"));
    return "home";
  }

  private String goToErrorPage(Model model, String message){
    model.addAttribute("message", message);
    return "error";
  }

  @PostMapping("/vote")
  public String vote(@ModelAttribute Voter voter, Model model) {

    Voter knownVoter = voterRepository.findByCnp(voter.getCnp());
    if (knownVoter == null){
      return goToErrorPage(model,"Invalid CNP");
    }
    if (!knownVoter.equals(voter)){
      return goToErrorPage(model,"Invalid data provided");
    }
    if (knownVoter.isVoted()){
      return goToErrorPage(model,"You have voted already");
    }

    knownVoter.setVoted(true);
    voterRepository.save(knownVoter);
    return "voted";
  }
}
