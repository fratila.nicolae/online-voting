package com.ubb.cripto.voting;

import javax.persistence.*;
import java.util.Objects;

@Entity
public class Voter {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private String cnp;

  private String name;

  private String address;

  @Column(unique = true)
  private String token;

  private boolean voted;

  private String choice;

  public String getCnp() {
    return cnp;
  }

  public void setCnp(String cnp) {
    this.cnp = cnp;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public String getToken() {
    return token;
  }

  public void setToken(String token) {
    this.token = token;
  }

  public boolean isVoted() {
    return voted;
  }

  public void setVoted(boolean voted) {
    this.voted = voted;
  }

  public String getChoice() {
    return choice;
  }

  public void setChoice(String choice) {
    this.choice = choice;
  }

  @Override
  public String toString() {
    return "Voter{" +
      "cnp='" + cnp + '\'' +
      ", name='" + name + '\'' +
      ", address='" + address + '\'' +
      ", token='" + token + '\'' +
      ", voted=" + voted +
      ", choice='" + choice + '\'' +
      '}';
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Voter voter = (Voter) o;
    return Objects.equals(cnp, voter.cnp) &&
      Objects.equals(name, voter.name) &&
      Objects.equals(address, voter.address) &&
      Objects.equals(token, voter.token);
  }

  @Override
  public int hashCode() {
    return Objects.hash(cnp, name, address, token);
  }
}
