package com.ubb.cripto.voting;

import org.springframework.data.repository.CrudRepository;

public interface VoterRepository extends CrudRepository<Voter, Long> {
    Voter findByCnp(String cnp);
}
